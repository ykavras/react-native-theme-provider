export type IColorPalette = {
  PRIMARY: IPaletteColors;
  SECONDARY: IPaletteColors;
  SUCCESS: IPaletteColors;
  WARNING: IPaletteColors;
  ERROR: IPaletteColors;
  GREY: IPaletteColors;
};

const COLOR_PALETTE: IColorPalette = {
  PRIMARY: {
    900: '#1D3557',
    800: '#274775',
    700: '#315992',
    600: '#3b6bb0',
    500: '#4e7fc4',
    400: '#6b94ce',
    300: '#89a9d8',
    200: '#a6bfe1',
    100: '#c4d4eb',
    50: '#e1eaf5',
  },
  SECONDARY: {
    900: '#457b9d',
    800: '#4e8ab1',
    700: '#6197ba',
    600: '#75a4c2',
    500: '#89b1cb',
    400: '#9cbed4',
    300: '#b0cbdc',
    200: '#c4d8e5',
    100: '#d8e5ee',
    50: '#ebf2f6',
  },
  SUCCESS: {
    900: '#2a9d8f',
    800: '#31b6a5',
    700: '#3bcab9',
    600: '#53d1c2',
    500: '#6cd8ca',
    400: '#84ded3',
    300: '#9de5dc',
    200: '#b5ebe5',
    100: '#cef2ed',
    50: '#e6f8f6',
  },
  WARNING: {
    900: '#ffb703',
    800: '#ffbe1c',
    700: '#ffc535',
    600: '#ffcd4f',
    500: '#ffd468',
    400: '#ffdb81',
    300: '#ffe29a',
    200: '#ffe9b3',
    100: '#fff1cd',
    50: '#fff8e6',
  },
  ERROR: {
    900: '#e63946',
    800: '#e84d58',
    700: '#eb616b',
    600: '#ed747d',
    500: '#f08890',
    400: '#f39ca2',
    300: '#f5b0b5',
    200: '#f8c4c7',
    100: '#fad7da',
    50: '#fdebec',
  },
  GREY: {
    900: '#6C757D',
    800: '#79838b',
    700: '#889198',
    600: '#979ea5',
    500: '#a6acb2',
    400: '#b5babf',
    300: '#c4c8cc',
    200: '#d2d6d8',
    100: '#e1e3e5',
    50: '#f0f1f2',
  },
};

const COMMON = {
  grey: COLOR_PALETTE.GREY,
  error: COLOR_PALETTE.ERROR,
  primary: COLOR_PALETTE.PRIMARY,
  success: COLOR_PALETTE.SUCCESS,
  warning: COLOR_PALETTE.WARNING,
  secondary: COLOR_PALETTE.SECONDARY,
};

const palette = {
  light: {
    ...COMMON,
    mode: 'light',
    text: {
      primary: COLOR_PALETTE.PRIMARY[900],
      secondary: COLOR_PALETTE.PRIMARY[500],
      disabled: COLOR_PALETTE.PRIMARY[600],
    },
    background: {
      default: '#f1f1f1',
      neutral: '#FFFFFF',
    },
  },
  dark: {
    ...COMMON,
    mode: 'dark',
    text: {
      primary: '#FFFFFF',
      secondary: COLOR_PALETTE.GREY[500],
      disabled: COLOR_PALETTE.GREY[600],
    },
    background: {
      default: '#141414',
      neutral: '#000000',
    },
  },
};

export default palette;

export interface IPaletteColors {
  '50': string;
  '100': string;
  '200': string;
  '300': string;
  '400': string;
  '500': string;
  '600': string;
  '700': string;
  '800': string;
  '900': string;
}

export interface IPaletteText {
  primary: string;
  secondary: string;
  disabled: string;
}

export interface IPaletteBackground {
  neutral: string;
  default: string;
}
