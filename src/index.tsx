import { Text, TextInput } from 'react-native';

// @ts-ignore
Text.defaultProps = Text.defaultProps || {};
// @ts-ignore
Text.defaultProps.allowFontScaling = false;

// @ts-ignore
TextInput.defaultProps = TextInput.defaultProps || {};
// @ts-ignore
TextInput.defaultProps.allowFontScaling = false;

export { ThemeTypesEnum, type IPaletteProps } from './contexts/types';

export {
  useTheme,
  ThemeProvider,
  useThemedStyles,
} from './contexts/ThemeContext';
