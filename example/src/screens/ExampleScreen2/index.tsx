import React from 'react';
import { View, Text, Button } from 'react-native';
import {
  useThemedStyles,
  useTheme,
  ThemeTypesEnum,
} from 'react-native-custom-theme-provider';
//
import styles, { type Styles } from './styles';

export default function ExampleScreen() {
  const { onChangeTheme, isDark } = useTheme();
  const style: Styles = useThemedStyles(styles);

  return (
    <View style={style.wrapper}>
      <Text style={style.title}>Custom Theme Provider</Text>
      <Text style={style.text}>React Native Custom Theme Provider</Text>
      <Button
        title="Change Theme"
        onPress={() => {
          onChangeTheme(isDark ? ThemeTypesEnum.LIGHT : ThemeTypesEnum.DARK);
        }}
      />
    </View>
  );
}
