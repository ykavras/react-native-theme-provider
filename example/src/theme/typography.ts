import { moderateScale } from 'react-native-size-matters';

type IFontWeight = '300' | '400' | '500' | '600' | '700';

// FONT FAMILY
const FONT_FAMILY_LIGHT = 'Poppins-Light';
const FONT_FAMILY_REGULAR = 'Poppins-Regular';
const FONT_FAMILY_MEDIUM = 'Poppins-Medium';
const FONT_FAMILY_SEMI_BOLD = 'Poppins-SemiBold';
const FONT_FAMILY_BOLD = 'Poppins-Bold';

// FONT WEIGHT
const FONT_WEIGHT_LIGHT: IFontWeight = '300';
const FONT_WEIGHT_REGULAR: IFontWeight = '400';
const FONT_WEIGHT_MEDIUM: IFontWeight = '500';
const FONT_WEIGHT_SEMI_BOLD: IFontWeight = '600';
const FONT_WEIGHT_BOLD: IFontWeight = '700';

// FONT WEIGHTS
export const FONT_WEIGHTS = {
  LIGHT: {
    fontFamily: FONT_FAMILY_LIGHT,
    fontWeight: FONT_WEIGHT_LIGHT,
  },
  REGULAR: {
    fontFamily: FONT_FAMILY_REGULAR,
    fontWeight: FONT_WEIGHT_REGULAR,
  },
  MEDIUM: {
    fontFamily: FONT_FAMILY_MEDIUM,
    fontWeight: FONT_WEIGHT_MEDIUM,
  },
  SEMI_BOLD: {
    fontFamily: FONT_FAMILY_SEMI_BOLD,
    fontWeight: FONT_WEIGHT_SEMI_BOLD,
  },
  BOLD: {
    fontFamily: FONT_FAMILY_BOLD,
    fontWeight: FONT_WEIGHT_BOLD,
  },
};

// FONT SIZE
export const FONT_SIZES = {
  40: { fontSize: moderateScale(40) },
  38: { fontSize: moderateScale(38) },
  36: { fontSize: moderateScale(36) },
  34: { fontSize: moderateScale(34) },
  32: { fontSize: moderateScale(32) },
  30: { fontSize: moderateScale(30) },
  28: { fontSize: moderateScale(28) },
  26: { fontSize: moderateScale(26) },
  24: { fontSize: moderateScale(24) },
  22: { fontSize: moderateScale(22) },
  20: { fontSize: moderateScale(20) },
  18: { fontSize: moderateScale(18) },
  16: { fontSize: moderateScale(16) },
  14: { fontSize: moderateScale(14) },
  12: { fontSize: moderateScale(12) },
  10: { fontSize: moderateScale(10) },
  8: { fontSize: moderateScale(8) },
};
