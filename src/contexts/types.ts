import type {
  IPaletteBackground,
  IPaletteColors,
  IPaletteText,
} from '../theme/palette';

export enum ThemeTypesEnum {
  DARK = 'dark',
  LIGHT = 'light',
}

export interface IPaletteProps {
  text: IPaletteText;
  mode: ThemeTypesEnum;
  grey: IPaletteColors;
  error: IPaletteColors;
  primary: IPaletteColors;
  success: IPaletteColors;
  warning: IPaletteColors;
  secondary: IPaletteColors;
  background: IPaletteBackground;
}

export type ThemeContextType = {
  isDark: boolean;
  colors: IPaletteProps;
  theme: ThemeTypesEnum;
  onChangeTheme(mode: ThemeTypesEnum): void;
};
