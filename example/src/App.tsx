import * as React from 'react';
import { ThemeProvider } from 'react-native-custom-theme-provider';
// Example Screens
import ExampleScreen from './screens/ExampleScreen';

export default function App() {
  return (
    <ThemeProvider>
      <ExampleScreen />
    </ThemeProvider>
  );
}
