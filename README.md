# React Native Custom Theme Provider

**_react-native-custom-theme-provider is a package designed to manage custom themes in your React Native projects. It includes additional features such as react-native-size-matters for responsive design._**

## Installation

To install the package and its dependencies, run the following commands:

```sh
yarn add react-native-custom-theme-provider react-native-size-matters @react-native-async-storage/async-storage
npx pod-install
```

# Important Notes

#### 1. Copying the Theme File:

Copy the theme file from the src folder in this repository to your own src folder in your project.

#### 2. Defining Font Family:

After installing the fonts in your project, define the 'FONT FAMILY' in the typography.ts file located in the theme folder.

## Why am I using these packages @react-native-async-storage/async-storage, @react-native-size-matters

#### 1. Why Use @react-native-async-storage/async-storage:

The @react-native-async-storage/async-storage package is employed to persistently store and manage critical application-specific data. In this package, it is specifically used to save and retrieve the user's theme preference (light or dark mode). By storing this preference, the application ensures a consistent theme across different sessions.

#### 2. Why Use @react-native-size-matters:

The inclusion of the @react-native-size-matters package is purposeful for achieving responsive design in React Native. This package facilitates the adaptation of components to various screen sizes, promoting a consistent and visually appealing layout across different devices. By utilizing functions like s and vs, which scale dimensions using relative units, the package ensures that the application's UI elements maintain proportionality and readability, enhancing the user experience.

## Usage

```js
import { ThemeProvider } from 'react-native-custom-theme-provider';

const App = () => {
  return <ThemeProvider>{/* ... */}</ThemeProvider>;
};

export default App;
```

## Usage Example Screen

```js
import React from 'react';
import { moderateScale } from 'react-native-size-matters';
import { View, StyleSheet, Text, TouchableOpacity } from 'react-native';
import {
  useTheme,
  ThemeTypesEnum,
  useThemedStyles,
  type IPaletteProps,
} from 'react-native-custom-theme-provider';
//

import { padding, margin } from './theme/mixins';
import { FONT_SIZES, FONT_WEIGHTS } from './theme/typography';

export default function ExampleScreen() {
  const { onChangeTheme, isDark } = useTheme();
  const style: Styles = useThemedStyles(styles);

  return (
    <View style={style.wrapper}>
      <Text style={style.title}>Custom Theme Provider</Text>
      <Text style={style.text}>React Native Custom Theme Provider</Text>
      <TouchableOpacity
        style={style.button}
        onPress={() => {
          onChangeTheme(isDark ? ThemeTypesEnum.LIGHT : ThemeTypesEnum.DARK);
        }}
      >
        <Text style={style.buttonTitle}>Change Theme</Text>
      </TouchableOpacity>
    </View>
  );
}

const styles = (colors: IPaletteProps, isDark: boolean) => {
  return StyleSheet.create({
    wrapper: {
      flex: 1,
      ...padding(16),
      alignItems: 'center',
      gap: moderateScale(16),
      justifyContent: 'center',
      backgroundColor: colors.background.default,
    },
    title: {
      ...FONT_SIZES[20],
      textAlign: 'center',
      ...FONT_WEIGHTS.SEMI_BOLD,
      color: colors.text.primary,
    },
    text: {
      ...FONT_SIZES[14],
      textAlign: 'center',
      ...FONT_WEIGHTS.REGULAR,
      color: colors.text.secondary,
    },
    button: {
      ...padding(0, 16),
      alignItems: 'center',
      justifyContent: 'center',
      height: moderateScale(44),
      borderRadius: moderateScale(10),
      backgroundColor: colors.background.neutral,
    },
    buttonTitle: {
      ...FONT_SIZES[12],
      ...FONT_WEIGHTS.SEMI_BOLD,
      color: colors.text.primary,
    },
    // Usage other colors
    usageColors: {
      color: colors.primary[400],
      backgroundColor: colors.grey[100],
    },
    // Usage is dark
    usageIsDark: {
      ...FONT_SIZES[14],
      ...FONT_WEIGHTS.REGULAR,
      color: isDark ? '#fff' : '#000',
    },
    // Usage Fonts
    usageFonts: {
      ...FONT_SIZES[8], // 8 between 40
      ...FONT_WEIGHTS.LIGHT,
      ...FONT_WEIGHTS.REGULAR,
      ...FONT_WEIGHTS.MEDIUM,
      ...FONT_WEIGHTS.SEMI_BOLD,
      ...FONT_WEIGHTS.BOLD,
    },
    // Usage margin
    usageMargin: {
      ...margin(10),
      ...margin(10, 20),
      ...margin(10, 20, 30),
      ...margin(10, 20, 30, 40),
    },
    // Usage padding
    usagePadding: {
      ...padding(10),
      ...padding(10, 20),
      ...padding(10, 20, 30),
      ...padding(10, 20, 30, 40),
    },
    // usage custom width,height
    usageCustomWidthHeight: {
      width: moderateScale(30),
      height: moderateScale(30),
    },
    // usage borderRadius
    usageBorderRadius: {
      borderRadius: moderateScale(30),
    },
    // usage other static values
    usageOtherStaticValues: {
      gap: moderateScale(16), // gap: 0.71 or later
      top: moderateScale(10),
      left: moderateScale(20),
      right: moderateScale(30),
      bottom: moderateScale(40),
      width: moderateScale(44),
      height: moderateScale(120),
      borderRadius: moderateScale(10),
      // ... other styles
    },
  });
};

type Styles = ReturnType<typeof styles>;
```

# Result

<img style="width: 200px; height: 400px; object-fit: cover;" src="https://gitlab.com/ykavras/react-native-theme-provider/-/raw/main/screenshoot-light.png" alt="React native custom theme provider screenshot light">
<img style="width: 200px; height: 400px; object-fit: cover;" src="https://gitlab.com/ykavras/react-native-theme-provider/-/raw/main/screenshoot-dark.png" alt="React native custom theme provider screenshot dark">
