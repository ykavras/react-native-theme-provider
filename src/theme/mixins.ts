import { moderateScale } from 'react-native-size-matters';

function dimensions(
  top: number,
  right: number = top,
  bottom: number = top,
  left: number = right,
  property?: string
) {
  const styles = {
    [`${property}Bottom`]: 0,
    [`${property}Left`]: 0,
    [`${property}Right`]: 0,
    [`${property}Top`]: 0,
  };

  styles[`${property}Top`] = moderateScale(top);
  styles[`${property}Right`] = moderateScale(right);
  styles[`${property}Bottom`] = moderateScale(bottom);
  styles[`${property}Left`] = moderateScale(left);

  return styles;
}

export function margin(
  top: number,
  right?: number,
  bottom?: number,
  left?: number
) {
  return dimensions(top, right, bottom, left, 'margin');
}

export function padding(
  top: number,
  right?: number,
  bottom?: number,
  left?: number
) {
  return dimensions(top, right, bottom, left, 'padding');
}
