import React from 'react';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { StatusBar, TextInput, useColorScheme, Platform } from 'react-native';
//
import palette from '../theme/palette';
import {
  ThemeTypesEnum,
  type IPaletteProps,
  type ThemeContextType,
} from './types';

const initializeTheme: ThemeContextType = {
  isDark: false,
  onChangeTheme: () => {},
  theme: ThemeTypesEnum.LIGHT,
  colors: palette.light as IPaletteProps,
};

const ThemeContext = React.createContext<ThemeContextType>(initializeTheme);

type Props = {
  children: React.ReactElement;
};

export function ThemeProvider({ children }: Props) {
  const colorScheme = useColorScheme() || ThemeTypesEnum.LIGHT;
  const [theme, setTheme] = React.useState<ThemeTypesEnum>(
    ThemeTypesEnum.LIGHT
  );
  const [loading, setLoading] = React.useState<boolean>(true);

  //
  const colors = palette[theme] as IPaletteProps;

  const onChangeTheme = async (mode: ThemeTypesEnum) => {
    setTheme(mode);
    await AsyncStorage.setItem('theme', mode);
  };

  const onInitializeTheme = React.useCallback(async () => {
    const getTheme = await AsyncStorage.getItem('theme');
    const mode = (getTheme || colorScheme) as ThemeTypesEnum;

    if (getTheme) {
      await onChangeTheme(mode);
    }

    setTheme(mode);
    setLoading(false);
  }, [colorScheme]);

  React.useEffect(() => {
    onInitializeTheme();
  }, [onInitializeTheme]);

  React.useEffect(() => {
    StatusBar.setBarStyle(
      theme === ThemeTypesEnum.DARK ? 'light-content' : 'dark-content',
      true
    );

    if (Platform.OS === 'android') {
      StatusBar.setBackgroundColor(colors.background.default);
    }

    if ([ThemeTypesEnum.LIGHT, ThemeTypesEnum.DARK].includes(theme)) {
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      TextInput.defaultProps.keyboardAppearance = theme;
    }
  }, [colors, theme]);

  const value = React.useMemo(() => {
    return {
      theme,
      colors,
      onChangeTheme,
      isDark: theme === ThemeTypesEnum.DARK,
    };
  }, [theme, colors]);

  if (loading) {
    return null;
  }

  return (
    <ThemeContext.Provider value={value}>{children}</ThemeContext.Provider>
  );
}

export const useTheme = () => React.useContext(ThemeContext);

//
export function useThemedStyles(styles: any) {
  const theme = useTheme();

  return styles(theme.colors, theme.isDark);
}
