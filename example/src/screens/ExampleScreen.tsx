import React from 'react';
import { moderateScale } from 'react-native-size-matters';
import { View, StyleSheet, Text, TouchableOpacity } from 'react-native';
import {
  useTheme,
  ThemeTypesEnum,
  useThemedStyles,
  type IPaletteProps,
} from 'react-native-custom-theme-provider';
//

import { padding, margin } from '../theme/mixins';
import { FONT_SIZES, FONT_WEIGHTS } from '../theme/typography';

export default function ExampleScreen() {
  const { onChangeTheme, isDark } = useTheme();
  const style: Styles = useThemedStyles(styles);

  return (
    <View style={style.wrapper}>
      <Text style={style.title}>Custom Theme Provider</Text>
      <Text style={style.text}>React Native Custom Theme Provider</Text>
      <TouchableOpacity
        style={style.button}
        onPress={() => {
          onChangeTheme(isDark ? ThemeTypesEnum.LIGHT : ThemeTypesEnum.DARK);
        }}
      >
        <Text style={style.buttonTitle}>Change Theme</Text>
      </TouchableOpacity>
    </View>
  );
}

const styles = (colors: IPaletteProps, isDark: boolean) => {
  return StyleSheet.create({
    wrapper: {
      flex: 1,
      ...padding(16),
      alignItems: 'center',
      gap: moderateScale(16),
      justifyContent: 'center',
      backgroundColor: colors.background.default,
    },
    title: {
      ...FONT_SIZES[20],
      textAlign: 'center',
      ...FONT_WEIGHTS.SEMI_BOLD,
      color: colors.text.primary,
    },
    text: {
      ...FONT_SIZES[14],
      textAlign: 'center',
      ...FONT_WEIGHTS.REGULAR,
      color: colors.text.secondary,
    },
    button: {
      ...padding(0, 16),
      alignItems: 'center',
      justifyContent: 'center',
      height: moderateScale(44),
      borderRadius: moderateScale(10),
      backgroundColor: colors.background.neutral,
    },
    buttonTitle: {
      ...FONT_SIZES[12],
      ...FONT_WEIGHTS.SEMI_BOLD,
      color: colors.text.primary,
    },
    // Usage other colors
    usageColors: {
      color: colors.primary[400],
      backgroundColor: colors.grey[100],
    },
    // Usage is dark
    usageIsDark: {
      ...FONT_SIZES[14],
      ...FONT_WEIGHTS.REGULAR,
      color: isDark ? '#fff' : '#000',
    },
    // Usage Fonts
    usageFonts: {
      ...FONT_SIZES[8], // 8 between 40
      ...FONT_WEIGHTS.LIGHT,
      ...FONT_WEIGHTS.REGULAR,
      ...FONT_WEIGHTS.MEDIUM,
      ...FONT_WEIGHTS.SEMI_BOLD,
      ...FONT_WEIGHTS.BOLD,
    },
    // Usage margin
    usageMargin: {
      ...margin(10),
      ...margin(10, 20),
      ...margin(10, 20, 30),
      ...margin(10, 20, 30, 40),
    },
    // Usage padding
    usagePadding: {
      ...padding(10),
      ...padding(10, 20),
      ...padding(10, 20, 30),
      ...padding(10, 20, 30, 40),
    },
    // usage custom width,height
    usageCustomWidthHeight: {
      width: moderateScale(30),
      height: moderateScale(30),
    },
    // usage borderRadius
    usageBorderRadius: {
      borderRadius: moderateScale(30),
    },
    // usage other static values
    usageOtherStaticValues: {
      gap: moderateScale(16), // 0.71 or later
      width: moderateScale(44),
      height: moderateScale(120),
      borderRadius: moderateScale(10),
    },
  });
};

type Styles = ReturnType<typeof styles>;
